packer {
  required_version = ">= 1.7.0"
  required_plugins {
    vsphere = {
      version = ">= 1.2.7"
      source  = "github.com/hashicorp/vsphere"
    }
    ansible = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/ansible"
    }
  }
}

variable "ansible_python_interpreter" {
  type    = string
  default = "/usr/bin/python"
}

variable "build_hash" {
  type    = string
  default = "${env("BUILD_HASH")}"
}

variable "build_hash_short" {
  type    = string
  default = "${env("BUILD_HASH_SHORT")}"
}

variable "build_os" {
  type    = string
  default = "${env("BUILD_OS")}"
}

variable "build_output" {
  type    = string
  default = "${env("BUILD_OUTPUT")}"
}

variable "installer-iso-url" {
  type = string
}

variable "installer-iso-sha256" {
  type = string
}

variable "installer-proxy" {
  type    = string
  default = "http://proxy.oit.duke.edu:3128"
}

variable "ks-iso-datastore" {
  type    = string
  default = "${env("KS_ISO_DATASTORE")}"
}

variable "template-username" {
  type    = string
  default = "${env("TEMPLATE_USERNAME")}"
}

variable "template-password" {
  type    = string
  default = "${env("TEMPLATE_PASSWORD")}"
}

variable "template-info-file" {
  type    = string
  default = "/etc/duke/template-info.txt"
}

variable "template-vmware-hardware-version" {
  type    = string
  default = "11"
}

variable "vsphere_cluster" {
  type    = string
  default = "${env("GOVC_CLUSTER")}"
}

variable "vsphere_datacenter" {
  type    = string
  default = "${env("GOVC_DATACENTER")}"
}

variable "vsphere_datastore" {
  type    = string
  default = "${env("GOVC_DATASTORE")}"
}

variable "vsphere_folder" {
  type    = string
  default = "${env("GOVC_FOLDER")}"
}

variable "vsphere_network" {
  type    = string
  default = "${env("GOVC_NETWORK")}"
}

variable "vsphere_password" {
  type    = string
  default = "${env("GOVC_PASSWORD")}"
}

variable "vsphere_resource_pool" {
  type    = string
  default = "${env("GOVC_RESOURCE_POOL")}"
}

variable "vsphere_server" {
  type    = string
  default = "${env("GOVC_URL")}"
}

variable "vsphere_username" {
  type    = string
  default = "${env("GOVC_USERNAME")}"
}

variable "template-os-version" {
  type = string
}

variable "vmware-guest-id" {
  type = string
}

variable "installer-volume-label" {
  type = string
}

variable "content-library" {
  type    = string
  default = "${env("GOVC_CONTENT_LIBRARY")}"
}

variable "final-template-name" {
  type    = string
  default = "${env("FINAL_TEMPLATE_NAME")}"
}

# source blocks are generated from your builders; a source can be referenced in
# build blocks. A build block runs provisioner and post-processors on a
# source. Read the documentation for source blocks here:
# https://www.packer.io/docs/templates/hcl_templates/blocks/source
source "vsphere-iso" "kali" {
  CPU_hot_plug         = true
  CPU_limit            = -1
  CPUs                 = 1
  RAM                  = "1024"
  RAM_hot_plug         = true
  boot_command         = ["c<wait>", "setparams 'packer-build'<enter>", "linux /install.amd/vmlinuz", " auto=true", " priority=critical", " console-setup/ask_detect=false", " debconf/frontend=noninteractive", " fb=false", " preseed/url=https://oit-ssi-systems.pages.oit.duke.edu/packer-kali-build/preseed.cfg", " noprompt quiet<enter>", "initrd /install.amd/initrd.gz<enter>", "boot<enter>"]
  boot_wait            = "15s"
  cluster              = "${var.vsphere_cluster}"
  datacenter           = "${var.vsphere_datacenter}"
  datastore            = "${var.vsphere_datastore}"
  disk_controller_type = ["pvscsi"]
  firmware             = "efi"
  folder               = "${var.vsphere_folder}"
  guest_os_type        = "${var.vmware-guest-id}"
  insecure_connection  = "true"
  iso_url              = "${var.installer-iso-url}"
  iso_checksum         = "${var.installer-iso-sha256}"
  network_adapters {
    network      = "${var.vsphere_network}"
    network_card = "vmxnet3"
  }
  password             = "${var.vsphere_password}"
  resource_pool        = "${var.vsphere_resource_pool}"
  ssh_private_key_file = "~/.ssh/id_ed25519"
  ssh_timeout          = "30m"
  ssh_username         = "packer"
  storage {
    disk_size = 51200
  }
  username       = "${var.vsphere_username}"
  vcenter_server = "${var.vsphere_server}"
  vm_name        = "packer-${var.template-os-version}-${var.build_hash_short}"
  vm_version     = "${var.template-vmware-hardware-version}"

  content_library_destination {
    name    = var.final-template-name
    library = var.content-library
    ovf     = true
    destroy = true
  }
}

# a build block invokes sources and runs provisioning steps on them. The
# documentation for build blocks can be found here:
# https://www.packer.io/docs/templates/hcl_templates/blocks/build
build {
  sources = ["source.vsphere-iso.kali"]

  provisioner "shell" {
    environment_vars = ["TEMPLATE_INFO_FILE=${var.template-info-file}", "BUILD_HASH=${var.build_hash}", "BUILD_HASH_SHORT=${var.build_hash_short}", "BUILD_OS=${var.build_os}"]
    execute_command  = "echo ${var.template-password} | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "/scripts/template-info.sh"
  }

  provisioner "shell" {
    environment_vars = ["TEMPLATE_USERNAME=${var.template-username}"]
    execute_command  = "echo ${var.template-password} | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "/scripts/prep-ansible.sh"
  }

  provisioner "ansible" {
    user            = "${var.template-username}"
    extra_arguments = ["--become", "-e","ansible_python_interpreter=${var.ansible_python_interpreter}","-e", "ansible_remote_tmp=/tmp","--scp-extra-args", "'-O'"]
    playbook_file   = "playbooks/packer-provisioning/packer-kali.yaml"
    use_proxy       = false
  }

  post-processor "manifest" {
    output     = "${var.build_output}/manifest.json"
    strip_path = true
  }
}
